<?php get_template_part('templates/page', 'header'); ?>
<section class="content">
  <div class="container">
    <div class="alert alert-warning">
      <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
    </div>
  </div>
</section>