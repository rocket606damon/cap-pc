<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


//* Extending Search to Include Custom Fields
function lp_cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {
    	if (strpos(strtolower($join), strtolower("left join ".$wpdb->postmeta. " ON")) === false){
        	$join .=" LEFT JOIN ".$wpdb->postmeta. " ON ". $wpdb->posts . ".ID = " . $wpdb->postmeta . ".post_id ";
	    }
    }

    return $join;
}
add_filter('posts_join', 'lp_cf_search_join' );
function lp_cf_search_where( $where ) {
    global $pagenow, $wpdb;
	$type = gettype($where);
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
		$where = str_replace("cpc_postmeta.post_id IS NULL", "9=9" , $where);
    }
    return $where;
}
add_filter( 'posts_where', 'lp_cf_search_where' );
function lp_cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'lp_cf_search_distinct' );