/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        
		function galleryHeight(){
			var txtHeight = $(".gallery-right .gall-top-text.green-bg").outerHeight();
			$(".small-image").css("height",txtHeight);
			var gallRight = $(".gallery-right").outerHeight();
			$(".gallery-left").css("height",gallRight);
		}
		
		function int_galleryHeight(){
			var txtHeight2 = $(".gall-txt-box.clinical-box").outerHeight();
			$(".image-case1").css("height",txtHeight2);
			var gallBottom = $(".gall-txt-box.reachtraining-box").outerHeight();
			$(".image-case2").css("height",gallBottom);
			$(".image-case3").css("height",gallBottom);
		}
		
		$(document).ready(function () {
			$(".probox-support").equalHeight();
			$(".color-bdr").equalHeight();    
		});
		
		(function($){
			$.fn.equalHeight = function (option) {
				var $this = this;
				var get_height = function(){
					var maxheight=0;
					$this.css("height","");
					$this.each(function(){
						maxheight = $(this).height() > maxheight ? $(this).height() : maxheight;
					});
					$this.height(maxheight);
				};
				var init =function(){
					get_height();
					$(window).bind("resize",get_height);
				};
				$this.destroy = function(){
					$this.css("height","");
					$(window).unbind("resize",get_height);
				};
				init();
				return this;
			};
		})(jQuery);
		
		$(window).load(function(){
			galleryHeight();
			int_galleryHeight();
			$(".color-bdr").equalHeight();
		});
		
		$(window).resize(function(){
			galleryHeight();
			int_galleryHeight();
			$(".color-bdr").equalHeight();
		});

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
