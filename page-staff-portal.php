<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'staff-portal'); ?>
  <?php get_template_part('templates/content', 'common'); ?>
<?php endwhile; ?>
