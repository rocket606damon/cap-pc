<section class="content">
  <div class="container">
      <?php
      if( have_rows('content') ):
      while ( have_rows('content') ) : the_row();
      if( get_row_layout() == 'one_column' ):
      ?>
      <div class="row">
        <div class="col-sm-12">
            <?php the_sub_field('content'); ?>
        </div>
      </div>
	  <?php elseif( get_row_layout() == 'two_columns' ): ?>
      <div class="row">
        <div class="col-sm-6">
            <?php the_sub_field('left_content'); ?>
        </div>
        <div class="col-sm-6">
            <?php the_sub_field('right_content'); ?>
        </div>
      </div>
      <?php elseif( get_row_layout() == 'accordion'): ?>
      <div class="accordion">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php $count = 0; ?>
        <?php if(have_rows('accordion_segment')):
        while (have_rows('accordion_segment')) : the_row(); ?>
        
            <div class="panel panel-default">
                <div class="panel-heading"<?php if(get_sub_field('background_color')) echo ' style="background-color: ' . get_sub_field('background_color') . ';"'; ?>>
                    <h3 class="panel-title"><a data-toggle="collapse"<?php if($count != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
                </div>
                <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse <?php if($count != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
                    <div class="panel-body">
                      <?php the_sub_field('content'); ?>
                    </div>
                </div>
            </div>
            
        <?php $count++;endwhile;endif; ?>
        </div>
      </div>
      <?php elseif( get_row_layout() == 'featured_content_2_columns'): ?>
          </div>
        </section>
        <section class="content-featured">
          <div class="row">  
            <?php if(get_sub_field('title')): ?><h2 class="border"><span><?php the_sub_field('title'); ?></span></h2><?php endif; ?>
            <?php  if( have_rows('featured-content') ): ?>
            <div class="container">
              <div class="row featured-box row-eq-height">
				<?php 
					while ( have_rows('featured-content') ) : the_row(); 
					$image = get_sub_field('thumbnail');
					if( !empty($image) ):
						$url = $image['url'];
						$size = 'thumbnail';
						$thumb = $image['sizes'][ $size ];
					endif;
				?>
            	<div class="col-sm-6">
                  <div class="probox-support">
                    <img class="img-circle" src="<?php echo $thumb; ?>" alt="Thumbnail">
                    <?php the_sub_field('content'); ?>
                  </div>
                </div>
            	<?php endwhile; ?>
              </div>
			<?php endif; ?>
        </section>
        <section class="content">
          <div class="container">
	  <?php elseif( get_row_layout() == 'featured_content_3_columns'): ?>
          </div>
        </section>
        <section class="content-featured">
          <div class="row">  
            <?php if(get_sub_field('title')): ?><h2 class="border"><span><?php the_sub_field('title'); ?></span></h2><?php endif; ?>
            <?php  if( have_rows('featured-content') ): ?>
            <div class="container">
              <div class="row featured-box row-eq-height">
				<?php while ( have_rows('featured-content') ) : the_row(); ?>
            	<div class="col-sm-4 ">
                  <div class="probox-support <?php the_sub_field('icon'); ?>">
                    <span class="icon-probox"></span>
                    <p><?php the_sub_field('content'); ?></p>
                  </div>
                </div>
            	<?php endwhile; ?>
              </div>
			<?php endif; ?>
        </section>
        <section class="content">
          <div class="container">
      <?php elseif( get_row_layout() == 'team_list'): ?>
      <div class="row">
        <div class="col-sm-12">
        <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
        <?php if(get_sub_field('fluid_columns')) { $columnClass = ''; } else { $columnClass = ' class="col-sm-4"';} ?>
		<?php if( have_rows('team_members') ): ?>
        <table class="table table-bordered table-striped table-responsive">
          <tbody>
          <?php 
		  	while ( have_rows('team_members') ) : the_row(); 
		  	$slug =  Roots\Sage\Extras\custom_slug(get_sub_field('name'));
		  ?>
              <tr>
              	<?php if(get_sub_field('title')): ?><td<?php echo $columnClass; ?>><?php the_sub_field('title'); ?></td><?php endif; ?>
                <?php if(get_sub_field('bio')): ?>
                <td<?php echo $columnClass; ?>><a href="#" data-toggle="modal" data-target="#<?php echo $slug; ?>"><?php the_sub_field('name'); ?></a></td>
                <?php else: ?>
                <td<?php echo $columnClass; ?>><?php the_sub_field('name'); ?></td>
                <?php endif; ?>
                <?php if(get_sub_field('phone')): ?><td<?php echo $columnClass; ?>><?php the_sub_field('phone'); ?></td><?php endif; ?>
                <?php if(get_sub_field('email')): ?><td<?php echo $columnClass; ?>><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></td><?php endif; ?>
            </tr>
          <?php endwhile; ?>
          </tbody>
      	</table>
		<?php endif; ?>
        </div>
      </div>
      <?php  if( have_rows('team_members') ): ?>
      <div class="row">
      	<div class="col-sm-12">
		  <?php 
            while ( have_rows('team_members') ) : the_row(); 
            $slug =  Roots\Sage\Extras\custom_slug(get_sub_field('name'));
          ?>
          <div class="modal team-bio" id="<?php echo $slug; ?>" role="dialog" aria-labelledby="<?php echo $slug; ?>Label">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="<?php echo $slug; ?>Label"><h3><?php the_sub_field('name'); ?></h3></h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <?php if(get_sub_field('image')): ?>
                    <div class="col-sm-3 image">
                        <img class="img-rounded img-responsive" src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>">
                    </div>
                    <div class="col-sm-9">
                        <?php the_sub_field('profile'); ?>
                    </div>
                    <?php else: ?>
                     <div class="col-sm-12">
                     <?php the_sub_field('profile'); ?>
                     </div>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endwhile; endif; ?>
	  	</div>
      </div>
	  <?php endif; endwhile; endif; ?>
      
      <?php 
	  /* Helful Links */
	  if( have_rows('resource') ): 
	  ?>
      <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <tbody>
            	<?php 
					while ( have_rows('resource') ) : the_row(); 
					if(get_sub_field('resource_type') === 'link'):
						$link = get_sub_field('link');
						$icon = 'fa-link';
					else:
						$link = get_sub_field('file');
						$icon = 'fa-file-o';
					endif;
				?>
                <tr>
                    <td>
                    	<?php if(get_sub_field('resource_type') == 'title'): ?>
                        <p><strong><?php the_sub_field('title'); ?></strong></p>
                        <?php else: ?>
                    	<p<?php if(get_sub_field('indent')) echo ' style="margin-left: 30px;"'; ?>><i class="fa <?php echo $icon; ?>" aria-hidden="true"></i> <a href="<?php echo $link; ?>" target="_blank"><?php the_sub_field('title'); ?></a></p>
                    	<?php if(get_sub_field('description')): ?><p><?php the_sub_field('description'); ?></p><?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
      </div>
	  <?php endif; ?>
          
      <?php 
		  /* Events */
		  if( have_rows('event') ): 
	  ?>
	  <?php while ( have_rows('event') ) : the_row(); ?>
      <div class="panel panel-info">
      	<div class="panel-heading"><h3 class="panel-title"><span class="badge"><?php the_sub_field('date'); ?></span> <?php the_sub_field('event_title'); ?></h3></div>
        <div class="panel-body"><?php the_sub_field('details'); ?></strong></div>
      </div>         
	  <?php endwhile; endif; ?>
        
	  <?php 
      /* Newsletters */
      if( have_rows('newsletter') ): 
      ?>
        <ul class="list-group">
        <?php while ( have_rows('newsletter') ) : the_row(); ?>
        <li class="list-group-item"><p><i class="fa fa-file-text-o" aria-hidden="true"></i> <a href="<?php the_sub_field('file'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></p></li>
        <?php endwhile; ?>
        </ul>
      <?php endif; ?>
      
    </div>
</section>