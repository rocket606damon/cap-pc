<?php if(!is_front_page()): ?>
<div class="int-gallery">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-5 eveningbox no-padding">
        <div class="gall-txt-box evening-box">
          <h2><?php the_field('green_block_title', 'option'); ?></h2>
          <a title="<?php the_field('green_block_link_text', 'option'); ?>" class="lnk-typ1" href="<?php the_field('green_block_link', 'option'); ?>"><?php the_field('green_block_link_text', 'option'); ?></a> </div>
      </div>
      <div class="col-xs-2 imagecase1 no-padding">
        <div class="image-case1"></div>
      </div>
      <div class="col-xs-5 clinicalbox no-padding">
        <div class="gall-txt-box clinical-box">
          <h2><?php the_field('orange_block_title', 'option'); ?></h2>
          <a title="<?php the_field('orange_block_link_text', 'option'); ?>" class="lnk-typ1" href="<?php the_field('orange_block_link', 'option'); ?>"><?php the_field('orange_block_link_text', 'option'); ?></a> </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-2 imagecase2 no-padding">
        <div class="image-case2"></div>
      </div>
      <div class="col-xs-5 reachtrainingbox no-padding">
        <div class="gall-txt-box reachtraining-box">
          <h2><?php the_field('blue_block_title', 'option'); ?></h2>
          <a title="<?php the_field('blue_block_link_text', 'option'); ?>" class="lnk-typ1" href="<?php the_field('blue_block_link', 'option'); ?>"><?php the_field('blue_block_link_text', 'option'); ?></a> </div>
      </div>
      <div class="col-xs-5 imagecase3 no-padding">
        <div class="image-case3"></div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if(have_rows('partners', 'option')): ?>
<section class="partners">
  <h2 class="border"><span>Project TEACH has the Support of</span></h2>
  <div class="container">
  	<?php while (have_rows('partners', 'option')) : the_row(); ?>
    <div class="col-sm-4"><div class="partner-img"><a href="<?php the_sub_field('url'); ?>" title="<?php the_sub_field('title'); ?>" target="_blank"><img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>"></a></div></div>
	<?php endwhile; ?>
  </div>
</section>
<?php endif; ?>