<footer class="content-info">
  <div class="container">
      <div class="ft-logo"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php the_field('footer_logo','option'); ?>" alt="<?php bloginfo('name'); ?>"></a></div>
      <div class="ft-no"><?php the_field('footer_phone', 'option'); ?></div>
      <div class="ft-txt"><?php bloginfo('name'); ?> &bull; <?php bloginfo('description'); ?></div>
      <div class="ft-search">
        <?php get_search_form(); ?>
      </div>
      <div class="copyright">&copy; <?php echo date("Y") ?> CAP PC NY</div>
  </div>
</footer>
