<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="topbar-left"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>" class="logo"><img src="<?php the_field('main_logo','option'); ?>" alt="<?php bloginfo('name'); ?>"></a>
          <div class="project-teach">
            <div class="logo-text">a regional provider for</div>
            <a href="https://www.omh.ny.gov/omhweb/project_teach/" title="Project TEACH" target="_blank"><img src="<?php bloginfo('url'); ?>/media/ProjectTeach-horizontal-logo.svg" alt="Project TEACH" width="180" height="auto"></a>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="topbar-right">
          <div class="phone-no">
          	<span class="big-no"><?php the_field('phone_1', 'option'); ?></span><br>
            <span class="small-no"><?php the_field('phone_2', 'option'); ?></span>
          </div>
          <div class="top-search"><?php get_search_form(); ?></div>
          <ul class="sub-menu">
            <li><a href="#"  data-toggle="modal" data-target="#cappc-suggestion"><i class="icon icon-ballot" aria-hidden="true"></i> Suggestion Box</a></li>
            <li>|</li>
          	<li><a href="https://www.cappcny.org/clincomm" target="_blank"><i class="fa fa-sign-in" aria-hidden="true"></i> Staff Portal</a></li>
            <li>|</li>
            <li><a href="<?php bloginfo('url'); ?>/contact/"><i class="fa fa-envelope" aria-hidden="true"></i> Contact</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="navigation-bg">
  <div class="container">
     <nav class="navbar navbar-default nav-primary">
      <div class="container-fluid"> 
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-nav" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="primary-nav">
          <?php
		  if (has_nav_menu('primary_navigation')) :
			wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
		  endif;
		  ?>
        </div>
      </div>
    </nav>     
  </div>
</div>

<div class="modal" id="cappc-suggestion" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <h3 class="modal-title" id="myModalLabel">Suggestions for CAP PC</h3>
        </div>
        <div class="modal-body">
          <?php echo do_shortcode('[gravityform id=5 title=false description=false ajax=true]'); ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>