<?php use Roots\Sage\Titles; ?>
<section class="int-banner">
  <div class="container">
    <div class="banner-txt">
      <h1><?= Titles\title(); ?></h1>
    </div>
  </div>
</section>
<div class="breadcrumb-wrap">
  <div class="container">
  	<?php Roots\Sage\Extras\custom_breadcrumbs(); ?>
  </div>
</div>