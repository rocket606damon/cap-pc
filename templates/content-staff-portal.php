<section class="content">
  <div class="container">
		
    <form class="form-signin">
      <h2 class="form-signin-heading">Please sign in</h2>
      <label class="sr-only" for="inputLoginID">Login ID:</label>
      <input type="email" autofocus="" required="" placeholder="Login ID" class="form-control" id="inputLoginID">
      <label class="sr-only" for="inputPassword">Password</label>
      <input type="password" required="" placeholder="Password" class="form-control" id="inputPassword">
      <div class="checkbox">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
    </form>
      
    </div>
</section>