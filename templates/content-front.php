<section class="featured">
  <div class="container">
    <div class="row clearfix">
      <div class="col-sm-4">
        <div class="pro-box clinical-box">
          <div class="color-bdr">
            <div class="pro-icon"></div>
            <h3><?php the_field('left_block_title'); ?></h3>
            <p><?php the_field('left_block_description'); ?></p>
            <a href="<?php the_field('left_block_page_link'); ?>" title="<?php the_field('left_block_page_link_text'); ?>" class="btn"><?php the_field('left_block_page_link_text'); ?></a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="pro-box educational-box">
          <div class="color-bdr">
            <div class="pro-icon"></div>
            <h3><?php the_field('center_block_title'); ?></h3>
            <p><?php the_field('center_block_description'); ?></p>
            <a href="<?php the_field('center_block_page_link'); ?>" title="<?php the_field('center_block_page_link_text'); ?>" class="btn"><?php the_field('center_block_page_link_text'); ?></a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="pro-box quarterly-box">
          <div class="color-bdr">
            <div class="pro-icon"></div>
            <h3><?php the_field('right_block_title'); ?></h3>
            <p><?php the_field('right_block_description'); ?></p>
            <a href="<?php the_field('right_block_page_link'); ?>" title="<?php the_field('right_block_page_link_text'); ?>" class="btn"><?php the_field('right_block_page_link_text'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php if(get_field('about_text')): ?>
<section class="map-control">
  <div class="container">
    <div class="row">
      <?php if(get_field('about_image')): ?>
      <div class="col-sm-7"><img src="<?php the_field('about_image'); ?>" alt="About CAP PC"></div>
      <div class="col-sm-5">
          <?php the_field('about_text'); ?>
          <?php if(get_field('about_link')): ?><div class="map-btn text-right"><a href="<?php the_field('about_link'); ?>" title="<?php the_field('about_link_text'); ?>" class="btn"><?php the_field('about_link_text'); ?></a></div><?php endif; ?>
      </div>
      <?php else: ?>
      <div class="col-sm-5">
          <?php the_field('about_text'); ?>
          <div class="map-btn text-right"><a href="<?php bloginfo('url'); ?>/the-program/" title="Keep Reading" class="btn">Keep Reading</a></div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php endif; ?>

<div class="toolbox-control">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="inner-box webinars"><a href="<?php the_field('left_link'); ?>" class="tool-icon" title="<?php the_field('left_link_title'); ?>"></a>
          <h2><a href="<?php the_field('left_link'); ?>" title="<?php the_field('left_link_title'); ?>"><?php the_field('left_link_title'); ?></a></h2>
          <a href="<?php the_field('left_link'); ?>" title="<?php the_field('left_link_text'); ?>" class="tool-link"><?php the_field('left_link_text'); ?></a> </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="inner-box reachtraining"><a href="<?php the_field('center_link'); ?>" class="tool-icon" title="<?php the_field('center_link_title'); ?>"></a>
          <h2><a href="<?php the_field('center_link'); ?>" title="<?php the_field('center_link_title'); ?>"><?php the_field('center_link_title'); ?></a></h2>
          <a href="<?php the_field('center_link'); ?>" title="<?php the_field('center_link_text'); ?>" class="tool-link"><?php the_field('center_link_text'); ?></a> </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="inner-box setting-box"><a href="<?php the_field('right_link'); ?>" class="tool-icon" title="<?php the_field('right_link_title'); ?>"></a>
          <h2><a href="<?php the_field('right_link'); ?>" title="<?php the_field('right_link_title'); ?>"><?php the_field('right_link_title'); ?></a></h2>
          <a href="<?php the_field('right_link'); ?>" title="<?php the_field('right_link_text'); ?>" class="tool-link"><?php the_field('right_link_text'); ?></a></div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="inner-box suggestion-box last"><a href="#" data-toggle="modal" data-target="#cappc-suggestion" class="tool-icon" title="suggestion-box"></a>
          <h2><a href="#" data-toggle="modal" data-target="#cappc-suggestion" title="suggestion-box">Suggestions for CAPPC</a></h2>
          <a href="#" data-toggle="modal" data-target="#cappc-suggestion" title="suggestion-box" class="tool-link">Learn More</a></div>
      </div>
    </div>
  </div>
</div>

<div class="gallery-control">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-5 mob-grid no-padding">
        <div class="gallery-left">  </div>
      </div>
      <div class="col-xs-7 mob-grid no-padding">
        <div class="gallery-right">
          <div class="gall-top-text">
            <h2><?php the_field('home_blue_block_title', 'option'); ?></h2>
            <a href="<?php the_field('home_blue_block_link', 'option'); ?>" class="lnk-typ1" title="<?php the_field('home_blue_block_link_text', 'option'); ?>"><?php the_field('home_blue_block_link_text', 'option'); ?></a> </div>
          <div class="row no-padding">
            <div class="col-xs-8 mob-grid no-padding">
              <div class="gall-top-text green-bg">
                <h2><?php the_field('home_green_block_title', 'option'); ?></h2>
                <p><?php the_field('home_green_block_link_text', 'option'); ?></p>
                </div>
            </div>
            <div class="col-xs-4 mob-grid no-padding">
              <div class="small-image"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_template_part('templates/content', 'common'); ?>