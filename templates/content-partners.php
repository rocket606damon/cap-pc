<?php if(have_rows('partners')): ?>
<section class="partners">
  <h2 class="border"><span>A SPECIAL THANKS TO OUR PARTNERS</span></h2>
  <div class="container">
  	<?php while (have_rows('partners')) : the_row(); ?>
    <div class="col-sm-4"><div class="partner-img"><a href="<?php the_sub_field('url'); ?>" title="<?php the_sub_field('title'); ?>" target="_blank"> <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>"></a></div></div>
	<?php endwhile; ?>
  </div>
</section>
<?php else: ?>
<section class="partners">
  <h2 class="border"><span>A SPECIAL THANKS TO OUR PARTNERS</span></h2>
  <div class="container">
  	<?php while (have_rows('partners', 2)) : the_row(); ?>
    <div class="col-sm-4"><div class="partner-img"><a href="<?php the_sub_field('url'); ?>" title="<?php the_sub_field('title'); ?>" target="_blank"> <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>"></a></div></div>
	<?php endwhile; ?>
  </div>
</section>
<?php endif; ?>